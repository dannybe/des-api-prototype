--
-- 'Hello World' data for dexter-api database 
--
\connect -reuse-previous=on "dbname='dexter-api-db'"

--
-- Data for Name: greeting; Type: TABLE DATA; Schema: public; Owner: postgres
--
COPY public.greeting (id, comment) FROM stdin;
1	Hello World
\.

--
-- Name: greeting_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--
SELECT pg_catalog.setval('public.greeting_id_seq', 1, false);

--
-- PostgreSQL database dump complete
--

