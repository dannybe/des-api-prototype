// Require all variables to be declared
'use strict';

// Default port application listens, can be overriden via environment variable
const port = process.env.port || 8080;

// Declare express web app framework variables
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json())

require('./routes/index.js')(app);

// Create a Server for the web app
var server = app.listen(port, function () {

  var host = server.address().address
  var port = server.address().port

  console.log("App listening at http://%s:%s", host, port)

})