// Setup postgres connection via environment variables
const Pool = require('pg').Pool
const pool = new Pool({
  user: process.env.POSTGRES_USER,
  host: process.env.PGHOST,
  database: process.env.POSTGRES_DB,
  password: process.env.POSTGRES_PASSWORD,
  port: process.env.PGPORT
})

// Function called via default HTTP GET
const getGreeting = (req, res) => {
  pool.query('SELECT comment FROM greeting where id = 1', (error, results) => {
    if (error) {
      console.error('Database connection failed: ' + error.stack);
      throw error
    }
    res.status(200).json(results.rows[0].comment)
  })
}

// Export functions for other methods
module.exports = {
  getGreeting,
}