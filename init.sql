--
-- Create initial table for dexter-api database
--
CREATE TABLE greeting (
    id SERIAL,
    comment varchar(255),
    PRIMARY KEY (id, comment),
    UNIQUE (id, comment)
);
