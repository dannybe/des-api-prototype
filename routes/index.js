module.exports = function (app) {

  const db = require('../controllers/queries.js');

  // Retrieve greeting
  app.get('/', db.getGreeting);
}
