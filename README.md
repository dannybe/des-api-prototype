# Dexter Energy Service Prototype

## Prerequisite
* NodeJS (with express and pg modules installed)
* Docker
* Docker Compose
~~* AWS account (For cloud deployment)~~
~~* Terraform installed (For cloud deployment)~~

## Local Build and Test of webapp

> git clone https://gitlab.com/dannybe/des-api-prototype.git
> cd des-api-prototype

modify .env variables as you like

> docker-compose up --build -d

The application (including postgres database) is now available via localhost	
Verify things are up using **docker-compose ps**
Check logs with **docker-compose logs** if you have issues

(Please note the database is empty and will need to be populated with at least a row of data to return the 'Hello World' drr below)

## Gitlab CI Pipeline

Any commits and/or pushes to the des-api-prototype repository automaticlly
creates new images and deploys to the Docker Hub registry

These images can be manually downloaded via docker pull or deployed to a Helm 
compatible Kubernetes environment (e.g. GCP, EKS, AKS, MiniKube, etc.) with
the Helm chart.

### Helm Chart Deployments

#### Validate Template

If you make any changes to files in the helm directory hierarchy, always run a '[helm template](https://helm.sh/docs/helm/helm_template/)' to validate the tempate and associated files. For example this:

```Bash
$ helm template dexter-api helm
```

Various variables such as postgres port, username, password, etc. can be overrriden as typical with helm chart values.yaml file.

#### Install

If this is the initial deployment, then create helm application for that namespace like:

```Bash
helm install des-devops helm
```

#### Upgrade

To upgrade already deployed chart use '[helm upgrade](https://helm.sh/docs/helm/helm_upgrade/)' to push changes to running k8s clusters.

```Bash
$ helm upgrade dexter-api helm
```

#### Overides

Using helm current variables can be overriden:
```Yaml
  postgres​:
    data​: /var/lib/postgresql/pgdata
    database: dexter-api-db​
    image_name: drbennett/postgres-db​
    mountPath​: /var/lib/postgresql/data
    name​: postgres-db
    port​: 5432
    replicas​: 1
    storage:​ 5Gi
    tag​: latest
  nodejs​:
    image_name:​ drbennett/dexter-api-app
    port​: 8080
    replicas​: 1
    tag​: latest
```
Please note the above values are the defaults. Override using a values.yaml file:
```Bash
$ helm upgrade dexter-api helm --valuesvalues.yaml
```

##### Validation

As mentioned above the database in the prototype is created empty. To add a single row of data to verify operation is required. THe repo includes a postgres data dump for just such a verification step. Use this command to add the validation data:

  ```Bash
  psql -h localhost -U postgres dexter-api-db < dexter-api.pgsql
  ```
